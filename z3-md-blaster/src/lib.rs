use id_ish::MaybeID;
use z3_md_data::chapter::Chapter;
use z3_md_data::title::Title;

pub enum Blast {
    Title(Title<()>),
    Chapter(Chapter<()>),
}
pub struct BlastEvt<ID: MaybeID> {
    /// The event id (for Last-Event-ID header, or the binary leading big-endian encoded form)
    pub update_id: ID,
    /// The ID of the internal value
    pub id: ID,
    /// The blast itself
    pub blast: crate::Blast,
}
