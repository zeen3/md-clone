use core::fmt;
#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub struct Part(u32);
impl From<u32> for Part {
    fn from(p: u32) -> Self {
        Self(if p == 0 { 1 } else { p << 4 })
    }
}
impl From<&mut u32> for Part {
    // hack
    fn from(p: &mut u32) -> Self {
        Self(*p)
    }
}
impl Part {
    // maxes out at using 27 bits
    const PART_MAX_DIGITS: usize = 8;
}
impl ::core::str::FromStr for Part {
    type Err = PartReadErr;
    /// Takes a "part" value and translates it into a meaningful value.
    /// Must be fed only the characters post the last dot value.
    ///
    /// Note: only leading characters here.
    fn from_str(s: &str) -> Result<Self, PartReadErr> {
        if s.len() == 0 {
            Ok(Self(0))
        } else {
            // remaining to consume
            let mut rem = Self::PART_MAX_DIGITS as u32;
            let mut trailing = 0;
            let val = s
                // ascii digits thanks
                .bytes()
                // read only ascii digits for sorting here
                .take_while(u8::is_ascii_digit)
                // couple of hacks happening here
                .try_fold(1u32, |acc, val| {
                    rem = rem.checked_sub(1).ok_or(ReadPartErr::Length)?;
                    let val = (val - b'0') as u32;
                    match val {
                        0 => trailing += 1,
                        _ => trailing = 0,
                    }
                    Ok(acc * 10 + val)
                });
            val.map(|v| v * u32::pow(10, rem))
                .map(|v| v - u32::pow(10, Self::PART_MAX_DIGITS as u32))
                .map(|v| (v << 4) | (trailing & 15))
                .map(Part)
        }
    }
}
impl fmt::Display for Part {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let n = self.0;
        if n == 0 {
            Ok(())
        } else {
            let (trailing, n) = (n & 15, n >> 4);
            struct Trailing(u32);
            impl fmt::Display for Trailing {
                fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                    let slic = [b'0'; 15];
                    let s = unsafe { core::str::from_utf8_unchecked(&slic[..self.0 as usize]) };
                    f.write_str(s)
                }
            }
            if n == 0 && trailing != 0 {
                write!(f, "{}", Trailing(trailing))
            } else {
                let mut hi = 0;
                while n % u32::pow(10, hi) == 0 {
                    hi += 1;
                    if hi > Self::PART_MAX_DIGITS as u32 {
                        return Ok(());
                    }
                }
                let val = n / u32::pow(10, hi - 1);
                write!(
                    f,
                    "{val:0>x$}{trail}",
                    val = val,
                    x = (Self::PART_MAX_DIGITS + 1 - hi as usize),
                    trail = Trailing(trailing)
                )
            }
        }
    }
}
impl fmt::Debug for Part {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(".").and_then(|()| fmt::Display::fmt(self, f))
    }
}
impl Into<u32> for Part {
    fn into(self) -> u32 {
        self.0
    }
}
impl Into<Option<u32>> for Part {
    fn into(self) -> Option<u32> {
        Some(self.0)
    }
}

pub struct PartReadErr(ReadPartErr);
impl fmt::Debug for PartReadErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&self.0, f)
    }
}
impl fmt::Display for PartReadErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}
impl ::std::error::Error for PartReadErr {}
#[derive(Clone, Copy, Debug)]
pub enum ReadPartErr {
    Overflow,
    Length,
    Empty,
}
impl fmt::Display for ReadPartErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ReadPartErr::*;
        f.write_str(match self {
            Overflow => "number too large to fit",
            Length => "number too long for this prescision",
            Empty => "number was empty",
        })
    }
}
impl From<ReadPartErr> for PartReadErr {
    fn from(r: ReadPartErr) -> Self {
        Self(r)
    }
}
