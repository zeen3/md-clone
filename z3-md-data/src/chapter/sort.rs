use super::part::Part;
use core::fmt;
#[cfg(feature = "debug_extras")]
use core::fmt::Write as _;
#[cfg(all(feature = "debug_extras", feature = "smartstring"))]
use smartstring::alias::String;

#[repr(C, align(8))]
#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub struct ChSort {
    // making a lot of presumptions here
    // oneshot uses min, empty uses max
    volume: i16,
    // even more here
    // oneshot uses min, empty uses max
    chapter: i16,
    // well fuck this is 9 places...
    // oneshot uses 0, empty uses 0, subtract 1 for
    // real value otherwise.
    part: u32,
    // TODO: abuse ziglang to make the bits a little more even:
    // - i17 for volume
    //   min -65535, max 65534
    //   1_ff_ff reserved for oneshot, 0_ff_ff for empty
    // - i22 for chapter
    //   min -4194303, max 4194303
    //   3f_ff_ff reserved for empty
    // - u27 for part (8 digits)
    //   134217727 max
    //   100000000 real values (00000000--99999999)
    //   00_00_00_00 reserved for empty
    //   note: sorting is done with a right-padded value, chaining
    //   the repeated character '0' is until 8 characters are consumed.
}

impl fmt::Debug for ChSort {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = f.debug_struct("ChSort");
        struct M<T: fmt::Debug>(Option<T>);
        impl<T: fmt::Debug> fmt::Debug for M<T> {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                if let Some(v) = self.0.as_ref() {
                    fmt::Debug::fmt(v, f)
                } else {
                    f.write_str("(empty)")
                }
            }
        }
        match self.parts() {
            None => s.field("is_oneshot", &true),
            Some((vol, ch, part)) => s
                .field("volume", &M(vol))
                .field("chapter", &M(ch))
                .field("subch", &M(part)),
        }
        .finish()
    }
}

impl ChSort {
    pub fn is_oneshot(&self) -> bool {
        Self::oneshot() == *self
    }
    /// an all-on (oneshot) chapter.
    pub const fn oneshot() -> Self {
        Self {
            volume: i16::MIN,
            chapter: i16::MIN,
            part: u32::MAX,
        }
    }
    pub fn tuple(self) -> (i16, i16, Part) {
        let (v, c, mut p) = self.tuple_raw();
        (v, c, Part::from(&mut p))
    }
    pub const fn tuple_raw(self) -> (i16, i16, u32) {
        let Self {
            volume,
            chapter,
            part,
        } = self;
        (volume, chapter, part)
    }
    pub fn parts(&self) -> Option<(Option<i16>, Option<i16>, Option<super::part::Part>)> {
        if self.is_oneshot() {
            None
        } else {
            let &Self {
                volume,
                chapter,
                part,
            } = self;
            let vol = match volume {
                i16::MIN | i16::MAX => None,
                _ => Some(volume),
            };
            let ch = match chapter {
                i16::MAX => None,
                _ => Some(chapter),
            };
            let part = part.checked_sub(1).map(super::part::Part::from);
            Some((vol, ch, part))
        }
    }
    #[cfg(feature = "debug_extras")]
    pub fn parts_string(self) -> String {
        match self.parts() {
            None => String::from("Oneshot"),
            Some((vol, ch, part)) => {
                let mut s = String::from("");
                macro_rules! f {
                    ($v:ident = $o:ident => $e:expr) => {
                        match $o {
                            Some($v) => $e,
                            None => Ok(()),
                        }
                    };
                }
                f!(v = vol => {
                    let f = if ch.is_some() { ", " } else { "" };
                    write!(s, "Vol. {}{}", v, f)
                })
                .and_then(|()| f!(c = ch => write!(s, "Ch. {}", c)))
                .and_then(|()| f!(p = part => write!(s, ".{}", p)))
                .unwrap();
                s
            }
        }
    }
    pub const fn from_bytes(b: [u8; 8]) -> Self {
        let volume = i16::from_le_bytes([b[0], b[1]]);
        let chapter = i16::from_le_bytes([b[2], b[3]]);
        let part = u32::from_le_bytes([b[4], b[5], b[6], b[7]]);
        Self {
            volume,
            chapter,
            part,
        }
    }
    pub fn to_bytes(&self) -> [u8; 8] {
        let &Self {
            volume,
            chapter,
            part,
        } = self;
        let (v, c, p) = (
            volume.to_le_bytes(),
            chapter.to_le_bytes(),
            part.to_le_bytes(),
        );
        [v[0], v[1], c[0], c[1], p[0], p[1], p[2], p[3]]
    }
    pub fn parts_for(
        vol: Option<i16>,
        ch: Option<i16>,
        part: Option<u32>,
    ) -> Result<(i16, i16, u32), NoSubpartCh> {
        Ok(match (vol, ch, part) {
            (Some(vol), Some(ch), Some(part)) => (vol, ch, part + 1),
            (Some(vol), Some(ch), None) => (vol, ch, 0),
            (Some(vol), None, None) => (vol, i16::MIN, 0),
            (None, Some(ch), Some(part)) => (i16::MAX, ch, part + 1),
            (None, Some(ch), None) => (i16::MAX, ch, 0),
            // things i don't want to deal with
            (None, None, None) => Self::oneshot().tuple_raw(),
            // orphaned subchapter (of volume)
            _ => return Err(NoSubpartCh(())),
        })
    }
    pub fn new_from_parts<Vol, Ch, Part>(vol: Vol, ch: Ch, part: Part) -> Result<Self, NoSubpartCh>
    where
        Vol: Into<Option<i16>>,
        Ch: Into<Option<i16>>,
        Part: Into<Option<u32>>,
    {
        Self::parts_for(vol.into(), ch.into(), part.into()).map(|(volume, chapter, part)| Self {
            volume,
            chapter,
            part,
        })
    }
}

#[derive(Clone, Copy)]
pub struct NoSubpartCh(());
impl fmt::Debug for NoSubpartCh {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("NoSubpartCh").finish()
    }
}
impl fmt::Display for NoSubpartCh {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Can't have an orphan subpart which lacks a chapter or volume")
    }
}
#[cfg(feature = "std")]
impl ::std::error::Error for NoSubpartCh {}

#[cfg(test)]
fn s<V: Into<Option<i16>>, C: Into<Option<i16>>, P: Into<Option<u32>>>(v: V, c: C, p: P) -> ChSort {
    ChSort::new_from_parts(v, c, p).unwrap()
}
#[test]
fn debug_chinfo() {
    dbg!(ChSort::oneshot());
    dbg!(ChSort::new_from_parts(1, 1, 0).unwrap());
    dbg!(ChSort::new_from_parts(1, 1, None).unwrap());
}

#[test]
fn sorts_properly() {
    let mut to_sort = vec![
        s(1, 1, None),
        s(1, 1, 0),
        ChSort::oneshot(),
        s(None, 5, None),
        s(1, 2, 0),
        s(2, 3, 1),
        s(1, 2, None),
    ];
    let expected = vec![
        ChSort::oneshot(),
        s(1, 1, None),
        s(1, 1, 0),
        s(1, 2, None),
        s(1, 2, 0),
        s(2, 3, 1),
        s(None, 5, None),
    ];
    to_sort.sort();
    dbg!(&expected);
    assert_eq!(to_sort, expected);
}
#[test]
#[cfg(feature = "debug_extras")]
fn string_parts_impl() {
    fn e(s: &'static str, ch: Result<ChSort, NoSubpartCh>) {
        let ch = ch.unwrap();
        let (_, ch) = dbg!(&ch, ch.parts_string());
        assert_eq!(s, ch);
    }
    e("Oneshot", Ok(ChSort::oneshot()));
    e("Vol. 1, Ch. 1", ChSort::new_from_parts(1, 1, None));
    e("Vol. 1, Ch. 1.0", ChSort::new_from_parts(1, 1, 0));
    e("Ch. 5", ChSort::new_from_parts(None, 5, None));
}
#[test]
#[should_panic]
fn ch_must_not_have_subch_with_empty_chapter_and_or_volume() {
    let (empty_ch, empty_vol) = dbg!(
        ChSort::new_from_parts(1, None, 1),
        ChSort::new_from_parts(None, None, 0)
    );
    empty_ch.or(empty_vol).map(|_| ()).unwrap()
}
