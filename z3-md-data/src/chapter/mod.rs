pub mod part;
pub mod sort;
pub type Repr = sort::ChSort;
pub struct Chapter<ID: ::id_ish::MaybeID = u64> {
    pub id: ID,
    pub title: String,
    // should really be a string-like value, or a wrapper around it.
    pub chapter_text: [u8; 8],
    // can't be easily generated
    sort: sort::ChSort,
}
use core::cmp::Ordering::{self, Equal};
impl<ID: ::id_ish::MaybeID + Ord> Ord for Chapter<ID> {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.sort.cmp(&other.sort) {
            Equal => self.id.cmp(&other.id),
            e => e,
        }
    }
}
impl<ID: ::id_ish::MaybeID + Ord> PartialOrd for Chapter<ID> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<ID: ::id_ish::MaybeID + Eq> Eq for Chapter<ID> {}
impl<ID: ::id_ish::MaybeID + Eq> PartialEq for Chapter<ID> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
