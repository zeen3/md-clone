
trait SepMeta {
    type Out;
    fn lang() -> bool;
    fn name() -> bool;
}
struct LangOnly;
impl SepMeta for LangOnly {
    type Out = ([u8; 4], String);
    fn lang() -> bool { true }
    fn name() -> bool { false }
}
struct SepMapLang;
impl SepMeta for SepMapLang {
    type Out = (String, Option<[NonZeroU8; 4]>, String);
    fn lang() -> bool { true }
    fn name() -> bool { true }
}
struct SingleSep;
impl SepMeta for SingleSep {
    type Out = String;
    fn lang() -> bool { false }
    fn name() -> bool { false }
}
struct SepMap;
impl SepMeta for SepMap {
    type Out = (String, String);
    fn lang() -> bool { false }
    fn name() -> bool { true }
}

struct NulLangSep<'a, M: SepMeta = LangOnly>(&'a str, PhantomData<M>);
impl<'a, M: SepMeta> Iterator for NulLangSep<'a, M> {
    type Item = <M as SepMeta>::Item;

}
