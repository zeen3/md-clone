use id_ish::MaybeID;
#[derive(Clone, Debug)]
pub struct Title<ID: MaybeID> {
    pub id: ID,
    /// Last (by "end of series", not including bonus pages) published/available chapter.
    pub last: crate::chapter::Repr,
    /// NUL separated colon-keyed lang:title pairs
    pub titles: String,
    /// NUL separated colon-keyed lang:titles
    pub blurbs: String,
    /// NUL separated author names.
    pub authors: String,
    /// NUL separated artist names
    pub artists: String,
    /// NUL separated external information (with defined routes). Follows name(.lang)?:data format.
    pub extinfo: String,
    /// Publication details
    pub publication: Publication,
    /// Tags/Genres
    pub tags: Tags,
    /// Lower and upper bounds of the chapters.
    pub chapter_spans: (u64, u64),
    /// Comment thread, and max id of comment.
    pub comment_thread: (u64, u64),
    /// Time the title was initially created.
    pub created: u64,
    /// Last updated information.
    pub last_updated: u64,
}
#[repr(C)]
#[derive(Debug, Clone)]
pub struct Publication {
    pub lang: serde_mangadex::langs::Language,
    pub status: serde_mangadex::manga_api::Status,
    pub demographic: Demographic,
}
#[derive(Debug, Copy, Clone)]
pub enum Demographic {}
#[repr(C, align(8))]
#[derive(Debug, Copy, Clone)]
pub struct Tags {
    pub tags: [u8; 16],
}
