#![feature(vec_into_raw_parts, atomic_mut_ptr)]
use core::{
    fmt,
    num::{NonZeroU32 as NZU32, NonZeroU64 as NZU64},
    sync::atomic::{AtomicU32 as U32, AtomicU64 as A64, Ordering::*},
};
use std::io;
#[repr(C)]
pub struct ReadMarker {
    /// user ID
    user: u64,
    /// title ID
    title: u64,
    /// current data (either bitwise epoch or ptr to bitwise epoch slice)
    dat: A64,
    /// maximum length of the data (default 1, >1 => diskptr)
    cap: U32,
    /// offset in user read pointer (0 is stored inline)
    user_offset: U32,
}
impl Default for ReadMarker {
    fn default() -> Self {
        Self::new(0, 0)
    }
}
impl Drop for ReadMarker {
    fn drop(&mut self) {
        let (cap, dat) = (*self.cap.get_mut(), *self.dat.get_mut());
        if cap != 1 {
            unsafe {
                let dat = Vec::from_raw_parts(dat as usize as *mut A64, cap as usize, cap as usize);
                drop(dat)
            }
        }
    }
}
impl ReadMarker {
    pub fn new(user: u64, title: u64) -> Self {
        Self {
            user,
            title,
            cap: U32::new(1),
            user_offset: U32::new(0),
            dat: A64::new(0),
        }
    }
    /// Marks a chapter (bit) as read.
    ///
    /// TODO: Work with >64 chapters.
    pub fn mark_read<A: AsRef<[u32]>>(&self, indexes: A) -> io::Result<()> {
        self.index_call(indexes.as_ref(), |ored, to_or| {
            to_or.fetch_or(ored, SeqCst);
        });
        Ok(())
    }
    /// Marks a chapter (bit) as unread.
    ///
    /// TODO: Work with >64 chapters.
    pub fn mark_unread<A: AsRef<[u32]>>(&self, indexes: A) -> io::Result<()> {
        self.index_call(indexes.as_ref(), |ored, to_or| {
            to_or.fetch_and(!ored, SeqCst);
        });
        Ok(())
    }
    fn markers_as_slice(&self) -> &[A64] {
        let (ptr, cap) = (self.dat.load(Acquire), self.cap.load(Acquire));
        if cap == 1 {
            // TODO: make this not be wildly unsafe
            unsafe { core::slice::from_raw_parts(self.dat.as_mut_ptr() as *mut A64, 1) }
        } else {
            unsafe { core::slice::from_raw_parts(ptr as usize as *mut A64, cap as usize) }
        }
    }
    /// Bumps size of the backing slice if it exists, or makes it exist if it doesn't.
    fn bump_to(&self, len: usize) {
        // TODO: make work with
        if len > 1 {
            let curr = self.markers_as_slice();
            if curr.len() < len {
                let mut v: Vec<u64> = Vec::with_capacity(len);
                let cap = v.capacity();
                let ptr = v.as_mut_ptr();
                unsafe {
                    ptr.copy_from_nonoverlapping(curr.as_ptr() as *const u64, curr.len());
                    for i in curr.len()..cap {
                        *ptr.add(i) = 0;
                    }
                    v.set_len(cap);
                }
                self.dat.store(ptr as usize as u64, Relaxed);
                self.cap.store(cap as u32, Relaxed);
                core::mem::forget(v);
                if curr.len() != 1 {
                    unsafe {
                        let ptr = curr.as_ptr() as *mut A64;
                        let old = Vec::from_raw_parts(ptr, curr.len(), curr.len());
                        drop(old);
                    }
                }
            }
        }
    }
    /// Calls `f` on each `(to_mark, current)` value.
    ///
    /// TODO: Extend the iterator after reaching the maximum value, and persist across more.
    fn index_call<F: FnMut(u64, &A64)>(&self, indexes: &[u32], mut f: F) {
        let (offset, ors) = Self::collect_indexes(indexes);
        let max = offset as usize + ors.len();
        self.bump_to(max.max(1));
        let markers = self.markers_as_slice().iter().skip(offset as _);
        ors.into_iter()
            // TODO: disk pointers and not using Once as a catch all
            .zip(markers)
            .for_each(|(ored, atomic)| f(ored, atomic))
    }
    fn collect_index(index: u32) -> (u32, smallvec::SmallVec<[u64; 1]>) {
        let bit = 1 << (index & 63);
        (index >> 6, smallvec::smallvec![bit])
    }
    fn collect_indexes(indexes: &[u32]) -> (u32, smallvec::SmallVec<[u64; 1]>) {
        // fast functions
        match indexes {
            [] => return (0, smallvec::smallvec![0]),
            [index] => return Self::collect_index(*index),
            _ => {}
        }
        let (min, max) = indexes
            .iter()
            .fold((u32::MAX, u32::MIN), |(min, max), &val| {
                (val.min(min), val.max(max))
            });
        let (lo, hi) = (
            // 0b..00111111 -> 0b..11000000 (0x..FFC0), cut the lowest 6 bits
            (min & !63),
            // 0b..00000000 -> 0b..00111111 (0x..003F), force lowest 6 bits
            (max | 63),
        );
        let pieces = hi - lo;
        if pieces >> 6 == 0 {
            // fast path: all within the same u64
            let ored = indexes
                .iter()
                .map(|&v| v - lo)
                .fold(0u64, |ored, v| ored | (1 << (v & 63)));
            (lo >> 6, smallvec::smallvec![ored])
        } else {
            // slow(er) path: split across multiple u64s.
            let mut data = vec![0u64; (pieces >> 6) as usize];
            indexes.iter().map(|&v| v - lo).for_each(|v| unsafe {
                let idx = (v >> 6) as usize;
                *data.get_unchecked_mut(idx) |= 1 << (v & 63);
            });
            (lo >> 6, smallvec::SmallVec::from_vec(data))
        }
    }
}
#[derive(Copy, Clone)]
enum MarkerDat {
    Ptr {
        cap: NZU32,
        user_offset: NZU32,
        ptr: NZU64,
    },
    Bit(u64),
}
impl fmt::Debug for MarkerDat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MarkerDat::Ptr {
                cap,
                user_offset,
                ptr,
            } => write!(f, "*+{}<{}>:", user_offset, cap).and_then(|()| unsafe {
                let bin = core::slice::from_raw_parts(ptr.get() as *mut u64, cap.get() as usize);
                for val in bin.iter() {
                    write!(f, " {:#X}", val)?;
                }
                Ok(())
            }),
            MarkerDat::Bit(b) => {
                if b == 0 {
                    f.write_str("<empty>")
                } else {
                    write!(f, "{:0>64b}", b)
                }
            }
        }
    }
}
impl MarkerDat {
    pub fn new(cap: u32, user_offset: u32, dat: u64) -> Self {
        if cap > 1 {
            let (cap, user_offset, ptr) = unsafe {
                (
                    NZU32::new_unchecked(cap),
                    NZU32::new_unchecked(user_offset),
                    NZU64::new_unchecked(dat),
                )
            };
            Self::Ptr {
                cap,
                user_offset,
                ptr,
            }
        } else {
            Self::Bit(dat)
        }
    }
}
impl fmt::Debug for ReadMarker {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        struct Underscore;
        impl fmt::Debug for Underscore {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                f.write_str("_")
            }
        }
        let (cap, user_offset, dat): (u32, u32, u64) = {
            let cap = self.cap.load(Acquire);
            let user_offset = self.user_offset.load(Acquire);
            let dat = self.dat.load(Acquire);
            (cap, user_offset, dat)
        };
        f.debug_struct("ReadMarker")
            .field("title_id", &self.title)
            .field("user_id", &self.user)
            .field("data", &MarkerDat::new(cap, user_offset, dat))
            .finish()
    }
}
#[test]
fn dbg_readmarker() {
    let rm = ReadMarker::new(1, 2);
    dbg!(&rm);
    rm.mark_read([0, 3, 4]).unwrap();
    dbg!(&rm);
    rm.mark_unread([2]).unwrap();
    dbg!(&rm);
    rm.mark_unread([4]).unwrap();
    dbg!(&rm);
    rm.mark_read([63]).unwrap();
    dbg!(&rm);
    rm.mark_read([64]).unwrap();
    dbg!(&rm);
    rm.mark_unread([63]).unwrap();
    dbg!(&rm);
}
