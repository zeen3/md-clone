//! # UUID ID-ish support
//!
//! UUID as a ID is supported...
use super::*;
use uuid::Uuid;
impl sealed::Sealed for Uuid {}
impl MaybeID for Uuid {
    type Out = u128;
    fn as_u128(&self) -> u128 {
        self.to_u128_le()
    }
    fn into_inner(self) -> Self::Out {
        MaybeID::as_u128(&self)
    }
}
impl ID for Uuid {}
