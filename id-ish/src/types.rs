//! # Types
//!
//! Implementations of the types for ID-ish.

use super::*;

macro_rules! id {
    (@seal $id:ty) => { impl sealed::Sealed for $id {} };
    (@maybe $id:ty ) => {
        id!(@seal $id);
        impl MaybeID for $id {
            type Out = $id;
            fn as_u128(&self) -> u128 {
                *self as _
            }
            fn into_inner(self) -> Self {
                self
            }
        }
    };
    (@transmute $id:ty => $out:ty) => {
        id!(@seal $id);
        impl MaybeID for $id {
            type Out = $out;
            fn as_u128(&self) -> u128 {
                MaybeID::into_inner(*self) as _
            }
            fn into_inner(self) -> Self::Out {
                 self as $out
            }
        }
    };
    (@nz $($sign:ty => $id:ty => $by:ty),+) => {
        $(
            id!(@seal $sign);
            id!(@seal $id);
            id!($id, $sign);
            impl MaybeID for $id {
                type Out = $by;
                fn as_u128(&self)-> u128 {
                    MaybeID::into_inner(*self) as u128
                }
                fn into_inner(self) -> Self::Out {
                    <$id>::get(self) as $by
                }
            }
            impl MaybeID for $sign {
                type Out = $by;
                fn as_u128(&self)-> u128 {
                    MaybeID::into_inner(*self) as u128
                }
                fn into_inner(self) -> Self::Out {
                    <$sign>::get(self) as $by
                }
            }
        )+
    };
    ($id:ty) => {
        impl ID for $id {}
    };
    ($($ids:ty),+) => { $(id!($ids);)+ };
    ($($sign:ty => $ids:ty),+) => {
        $(
            id!($ids);
            id!(@maybe $ids);
            id!(@transmute $sign => $ids);
        )+
    };
    (@peel $n:tt -> $A:ident, $($v:tt -> $B:ident),+) => {
        id!($($v -> $B),+);
    };
    ($($v:tt -> $B:ident)+) => {
        impl<$($B),+> sealed::Sealed for ($($B),+) where $($B: sealed::Sealed),+ {}
        impl<$($B),+> MaybeID for ($($B),+) where $($B: MaybeID),+ {
            type Out = ($(<$B as MaybeID>::Out),+);
            fn as_u128(&self) -> u128 {
                panic!("Tuples can't be converted to u128");
            }
            fn into_inner(self) -> Self::Out
            {
                ($(<$B as MaybeID>::into_inner(self.$v)),+)
            }
        }

        impl<$($B),+> ID for ($($B),+) where $($B: ID),+ {}
    };
}

id!(@seal ());
impl MaybeID for () {
    type Out = ();
    fn as_u128(&self) -> u128 {
        0
    }
    fn into_inner(self) -> Self {
        self
    }
}
id!(i8 => u8, i16 => u16, i32 => u32, i64 => u64, isize => usize);
id!(@nz NonZeroI8 => NonZeroU8 => u8, NonZeroI16 => NonZeroU16 => u16, NonZeroI32 => NonZeroU32 => u32, NonZeroI64 => NonZeroU64 => u64, NonZeroIsize => NonZeroUsize => usize);
#[cfg(feature = "u128")]
mod u_128 {
    use super::*;
    id!(i128 => u128);
    id!(@nz NonZeroU128 => NonZeroI128 => u128);
}

id! { 0 -> A 1 -> B }
id! { 0 -> A 1 -> B 2 -> C }
id! { 0 -> A 1 -> B 2 -> C 3 -> D }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G 7 -> H }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G 7 -> H 8 -> I }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G 7 -> H 8 -> I 9 -> J }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G 7 -> H 8 -> I 9 -> J 10 -> K }
id! { 0 -> A 1 -> B 2 -> C 3 -> D 4 -> E 5 -> F 6 -> G 7 -> H 8 -> I 9 -> J 10 -> K 11 -> L }
