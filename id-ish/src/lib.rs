//! # ID-ish
//!
//! Provides ID-ish types & traits for use in software.
//!
//!

use core::num::*;
mod sealed {
    pub trait Sealed: Copy + Sized {}
}
pub trait MaybeID: sealed::Sealed {
    type Out;
    fn as_u128(&self) -> u128;
    fn into_inner(self) -> Self::Out;
}
pub trait ID: MaybeID {}
mod types;

#[cfg(feature = "uuid")]
mod uuid_support;

pub mod phantom;

pub mod id_containers;
