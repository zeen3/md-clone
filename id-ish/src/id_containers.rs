//! # ID Container helpers
//!
//! In case you have an ID, that you need to pull from a map, and serialize (or deserialize and
//! throw into a map), this is somewhat useful.
//!
//!
//! TODO: ID-ish derive
//!
//! A toy implementation:
//!
//! ```no_run
//! # use id_ish::{*, id_containers::*};
//! struct Testing<ID: MaybeID> {
//!     id: ID,
//!     tx: &'static str,
//! }
//! impl<T: ID> WithID<T> for Testing<()> {
//!     type Output = Testing<T>;
//!     fn with_id(self, id: T) -> Testing<T> {
//!         Testing { tx: self.tx, id }
//!     }
//! }
//! impl<T: ID> TakeID for Testing<T> {
//!     type Output = Testing<()>;
//!     type ID = T;
//!     fn take_id(self) -> (T, Testing<()>) {
//!         (
//!             self.id,
//!             Testing {
//!                 id: (),
//!                 tx: self.tx,
//!             },
//!         )
//!     }
//! }
//! ```
//!
//! As you can see, the boilerplate is extremely hefty. If you wish to write a derive for it, eg
//! using a separate trait:
//!
//! ```rust,ignore
//! # trait HasID: WithID + TakeID {}
//! #[derive(HasID)]
//! struct Testing<ID> {
//!     id: ID,
//!     tx: &'static str,
//! }
//! ```
//!
//! that would be greatly appreciated. Please make contact via the repo, or contact
//! [id-ish at zeen3.xyz](mailto:id-ish@zeen3.xyz)

use super::*;

use core::iter::{FromIterator, IntoIterator};
/// The with-pair of a ID method.
pub trait WithID<T>: Sized
where
    T: ID,
{
    /// Value after including the ID.
    type Output: TakeID<ID = T, Output = Self>;
    fn with_id(self, id: T) -> Self::Output;
    #[doc(hidden)]
    #[inline(always)]
    /// Take-by-value tuple
    fn tup_with_id((id, this): (T, Self)) -> Self::Output {
        this.with_id(id)
    }
}
/// Simple implementation for a map-like to list method.
#[inline(always)]
pub fn map_to_list<K, V, C>(i: impl IntoIterator<Item = (K, V)>) -> C
where
    C: FromIterator<<V as WithID<K>>::Output>,
    K: ID,
    V: WithID<K>,
{
    C::from_iter(i.into_iter().map(WithID::tup_with_id))
}

/// The take-pair if a ID method.
pub trait TakeID: Sized {
    /// Value after removing the ID.
    type Output: WithID<Self::ID, Output = Self>;
    /// Value of the ID
    type ID: ID;
    fn take_id(self) -> (Self::ID, Self::Output);
}
/// Simple implementation for a list-like to map method.
#[inline(always)]
pub fn list_to_map<V, C>(i: impl IntoIterator<Item = V>) -> C
where
    C: FromIterator<(<V as TakeID>::ID, <V as TakeID>::Output)>,
    V: TakeID,
{
    C::from_iter(i.into_iter().map(TakeID::take_id))
}

#[cfg(feature = "with-take-id")]
/// The hackiest macro you'll ever lose.
/// Probably shouldn't be used. You can probably write one better yourself, quite honestly.
#[macro_export]
macro_rules! with_take_id {
    ($vis:vis struct $name:ident$(<$($args:tt,)*>)? {
        $(#[$id:meta])* $id_vis:vis id: $ty:ty,
        $($(#[$meta:meta])* $k_vis:vis $k:ident : $v:ty),*
    }) => {
        $vis struct $name<$($args,)* ID = $ty> {
            $(#[$id])* $id_vis id: ID,
            $($(#[$meta])* $k_vis $k: $v),*
        }
        impl<T: $crate::ID> $crate::id_containers::WithID<T> for $name<$($args,)* ()> {
            type Output = $name<$($args,)* T>;
            fn with_id(self, id: T) -> Self::Output {
                let $name { id: (), $($k,)* } = self;
                $name {
                    id, $($k,)*
                }
            }
        }
        impl<T: $crate::ID> $crate::id_containers::TakeID for $name<$($args,)* T> {
            type Output = $name<$($args,)* ()>;
            type ID = T;
            fn take_id(self) -> (Self::ID, Self::Output) {
                let $name { id, $($k,)* } = self;
                (id, $name {
                    id: (),
                    $($k,)*
                })
            }
        }
    }
}

/// Broken example type, for demonstation.
#[cfg(all(doc, feature = "with-take-id"))]
with_take_id! {
    pub struct DerivedTypeWithTakeIDAndWithID {
        pub id: u64,
        pub text: String
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[derive(Debug, Clone, Copy)]
    struct Testing<ID: MaybeID> {
        id: ID,
        tx: &'static str,
    }
    impl From<&'static str> for Testing<()> {
        fn from(tx: &'static str) -> Self {
            Self { id: (), tx }
        }
    }
    impl<T: ID> Testing<T> {
        fn new(id: T, tx: &'static str) -> Self {
            Self { id, tx }
        }
    }
    impl<T: ID> WithID<T> for Testing<()> {
        type Output = Testing<T>;
        fn with_id(self, id: T) -> Testing<T> {
            Testing { tx: self.tx, id }
        }
    }
    impl<T: ID> TakeID for Testing<T> {
        type Output = Testing<()>;
        type ID = T;
        fn take_id(self) -> (T, Testing<()>) {
            (
                self.id,
                Testing {
                    id: (),
                    tx: self.tx,
                },
            )
        }
    }
    #[test]
    fn test_map_to_list() {
        let hm = std::collections::HashMap::<u64, Testing<()>>::from_iter([
            (1, Testing::from("hoi")),
            (2, Testing::from("there")),
            (3, Testing::from("i'm")),
            (4, Testing::from("tem")),
        ]);
        let _: Vec<Testing<u64>> = dbg!(map_to_list(hm));
    }
    #[test]
    fn test_list_to_map() {
        let ls: [Testing<u64>; 4] = [
            Testing::new(1, "hoi"),
            Testing::new(2, "there"),
            Testing::new(3, "i'm"),
            Testing::new(4, "tem"),
        ];
        let _: std::collections::HashMap<u64, Testing<()>> = dbg!(list_to_map(ls));
    }
}
