//! # Phantom IDs
//!
//! IDs that are effectively the same as an empty tuple, but carry type information.
use super::*;
use core::marker::PhantomData;
pub trait PhantomID: sealed::Sealed {
    type ID: ID;
    fn to_id(self, id: Self::ID) -> Self::ID {
        id
    }
}
impl<T: ID> sealed::Sealed for PhantomData<T> {}
impl<T: ID> PhantomID for PhantomData<T> {
    type ID = T;
}
impl<T: ID> MaybeID for PhantomData<T> {
    type Out = ();
    fn as_u128(&self) -> u128 {
        0
    }
    fn into_inner(self) -> Self::Out {
        ()
    }
}
