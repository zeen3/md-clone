# ID-ish

Provides traits for ID types.

Provides two traits at the top level:

- `ID`
- `MaybeID`

And two at in `id_containers`:

- `WithID`
- `TakeID`

MaybeID has two methods:

- `as_u128`
- `into_inner`

ID has none, but builds on MaybeID.

# Stuff

- MaybeID has implementations on (), all integer types (i128/u128 optional), and uuid::Uuid (optional).
- ID has implementations on all integer types, and uuid::Uuid.
- WithID/TakeID have a default macro that "derives" a WithID/TakeID implementation that's valid for any structure with a leading `id` field.

