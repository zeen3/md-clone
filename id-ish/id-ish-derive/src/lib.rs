extern crate proc_macro;
use proc_macro::TokenStream;
use quote::{quote, quote_spanned};
use syn::{
    parse_macro_input, parse_quote, spanned::Spanned, Data, DeriveInput, Fields, GenericParam,
    Generics, Index,
};

#[proc_macro_derive(IDTakeWith, attributes(id))]
pub fn id_take_with(ts: TokenStream) -> TokenStream {
    let input = parse_macro_input!(ts as DeriveInput);
    let name = input.ident;
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let (id_ty, id_name) = do_id(&input.data);

    let expanded = quote! {
        impl #impl_generics ::id_ish::id_containers::WithID<#id_ty> for #name #ty_generics #where_clause {
            type Output = #name <#id_ty>;
            fn with_id(self, id: #id_ty) -> Self::Output {
                #name {
                    #id_name: id,
                    ..self
                }
            }
        }
        impl #impl_generics ::id_ish::id_containers::TakeID for #name #ty_generics #where_clause {
            type ID = #id_ty;
            type Output = #name<::core::marker::PhantomData<#id_ty>>;
            fn take_id(self) -> (Self::ID, Self::Output) {
                (self.#id_name, #name {
                    #id_name: ::core::marker::PhantomData,
                    ..self
                })
            }
        }
    };
    TokenStream::from(expanded)
}
fn do_id(data: &Data) -> (TokenStream, TokenStream) {
    if_chain! {
        let Data::Struct(ref data) = *data;
        let Fields::Named(ref fields) = data.fields;
        then {
            fields.named.iter().find(|field| {
                field.attrs.
            })
        }
    }
}
